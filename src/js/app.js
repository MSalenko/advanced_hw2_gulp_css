import * as flsFunctions from "./modules/functions.js";

flsFunctions.isWebp();

import Swiper, { Navigation, Pagination } from 'swiper';
const swiper = new Swiper();

// Код проекту
const button = document.querySelector('.button');
const dropMenu = document.querySelector('.menu');
const menuLogo = document.querySelector('.menu-icon');

function dropdownMenu() {
    menuLogo.classList.toggle('inactive');
    menuLogo.nextElementSibling.classList.toggle('inactive');
    dropMenu.classList.toggle('inactive');
}

button.addEventListener('click', dropdownMenu);